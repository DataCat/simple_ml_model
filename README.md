# simple_ml_model

[![pipeline status](https://gitlab.com/DataCat/simple_ml_model/badges/master/pipeline.svg)](https://gitlab.com/DataCat/simple_ml_model/-/commits/master)
[![coverage report](https://gitlab.com/DataCat/simple_ml_model/badges/master/coverage.svg)](https://gitlab.com/DataCat/simple_ml_model/-/commits/master)

This is the abstract ml project for testing [MLops pipeline](https://gitlab.com/DataCat/MLops_setup)

## Prerequisites

- docker

## Installation

```bash
pip3 install -r requirements.txt
```

## Usage

```bash
python3 app.py -h
```

## Usage via docker

```bash
docker build -t simple_ml:latest .
```

```bash
docker run simple_ml app.py -h
```
